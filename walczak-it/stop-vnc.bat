@echo off

sc config tvnserver start=disabled || goto :error
net stop tvnserver || goto :error
echo "Zdalny dostep do Twojego pulpitu zostaL wylaczony"
sleep 10
exit 0

:error
echo "Blad!!! Nie udalo sie na stale wylaczyc udostepniania pulpitu"
pause
exit %errorlevel%
